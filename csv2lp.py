#!/usr/bin/env python
#
"""
Convert a CSV file to the Line Protocol format used by InfluxDB. The
resulting file can then be loading into a database using the InfluxDB
command-line tool.
"""
from __future__ import print_function
import sys
import csv
import argparse


def write_header(outf, dbname):
    outf.write('# DDL\nCREATE DATABASE IF NOT EXISTS {0}\n\n'.format(dbname))
    outf.write('# DML\n# CONTEXT-DATABASE: {0}\n\n\n'.format(dbname))


def main():
    """
    Convert a CSV file to InfluxDB Line Protocol.
    """
    parser = argparse.ArgumentParser(description=main.__doc__,
                                     epilog=__doc__)
    parser.add_argument('dbname',
                        help='database name')
    parser.add_argument('meas',
                        help='measurement name')
    parser.add_argument('infile',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin,
                        help='input CSV file')
    parser.add_argument('-t', '--tag',
                        action='append',
                        dest='tags',
                        default=[],
                        help='store the named variable as a tag')
    parser.add_argument('-o', '--output',
                        type=argparse.FileType('w'),
                        default=sys.stdout,
                        metavar='FILE',
                        help='output file name')
    parser.add_argument('--time-var',
                        default='time',
                        help='name of the time-stamp column in the input')
    args = parser.parse_args()

    write_header(args.output, args.dbname)
    reader = csv.DictReader(args.infile)
    for row in reader:
        key = [args.meas] + ['='.join((t, row.pop(t))) for t in args.tags]
        tstamp = row.pop(args.time_var)
        fields = ','.join(['='.join((k, v)) for k, v in row.items()])
        print(','.join(key), fields, tstamp, file=args.output)


if __name__ == '__main__':
    main()
