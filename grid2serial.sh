#!/bin/bash
#
# Output real-time ICEX grid orientation to a serial port.
#
# Record format:
#
# YYYY-mm-dd HH:MM:SS LATITUDE LONGITUDE AZIMUTH
#
# Each record is terminated by carriage-return/line-feed.
#

usage() {
    echo "$(basename $1) [--baud N] serial_dev [serial_dev ...]" 1>&2
}

# Redis pub-sub channel to monitor
: ${GRID_CHANNEL=grid.update}

# We need GNU sed
if which gsed 1> /dev/null 2>&1; then
    SED=gsed
else
    SED=sed
fi

# ... and GNU date
if which gdate 1> /dev/null 2>&1; then
    DATE=gdate
else
    DATE=date
fi

if which gstdbuf 1> /dev/null 2>&1; then
    STDBUF=gstdbuf
else
    STDBUF=stdbuf
fi

prog="$0"
[[ $# == 0 ]] && {
    usage $prog
    exit 1
}

devs=()
baud=
while [[ $# != 0 ]]; do
    case "$1" in
        --baud|-b)
            baud="$2"
            shift 2
            ;;
        *)
            devs+=("$1")
            shift
            ;;
    esac
done

[[ "${#devs[@]}" == "0" ]] && {
    echo "No serial device specified" 1>&2
    usage $prog
    exit 1
}

# Configure the serial port tty
for dev in "${devs[@]}"; do
    [[ "$dev" = /dev/tty* ]] && stty $baud onlcr < "$dev"
done

while IFS=, read t lat lon az; do
    ts="$($DATE -u -d@$t +'%Y-%m-%d %H:%M:%S')"
    for dev in "${devs[@]}"; do
        echo "$ts $lat $lon $az" > $dev
    done
done < <($STDBUF -oL redis-cli --raw subscribe "$GRID_CHANNEL" |
                $SED -u -n '/^message/,+2p' |
                $SED -u -n '0~3p' |
                jq --unbuffered -c "[.timestamp, .lat, .lon, .azimuth]" |
                jq --unbuffered -r "@csv")
