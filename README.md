GICS Services
==============

This repository contains the templates for the
[Systemd](http://www.freedesktop.org/wiki/Software/systemd/) configuration
files for the various GICS services.

## gpsrdr.service

This service samples the Camp (Origin) GPS and the RF-GPS units and
publishes the data records on the following [Redis](http://redis.io/)
pub-sub channels:

+ *data.gps*
+ *data.rfgps*
+ *data.grid* (merged GPS and RF-GPS data)
+ *data.ranges*

## metrdr.service

This service reads data records from the Met Station and publishes the
data records to the *data.weather* channel.

## rngfilt.service

This service subscribes to the *data.ranges* channel and filters the data
using an exponential moving average filter. The filtered data is published
to *ranges.filtered*.

## gridcheck.service

This service subscribes to the *data.grid* channel and uses this
information along with the most recent survey reference points to
calculate the Grid azimuth and to check whether the RF-GPS units are
within a specified watch-circle. The new Grid parameters (origin and
azimuth) are published to the *grid.update* channel and any watch-circle
alerts are published to *grid.alert*.

## serialout.service

This service subscribes to the *grid.update* channel and writes the Grid
parameters to one or more serial ports.

## archiver.service

This service subscribes to the various data channels and archives the data
records in a series of CSV files.

## survey-app.service

This service provides a Web Application which subscribes to the
*ranges.filtered* channel and uses this information to survey the Grid;
establish its initial azimuth and the X-Y location of the RF-GPS units
(the survey reference points).

## gps-app.service

This service provides a Web Application which subscribes to the
*grid.update* and *data.gps* channels and presents this information to the
User. It also provides a form for converting between Grid and GPS
coordinates.

## weather-app.service

This service provides a Web Application which subscribes to the
*data.weather* channel and presents the Met Station data in tabular and
graphical form.
