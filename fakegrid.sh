#!/bin/bash
#
# Generate a fake Grid data stream
#
INTERVAL=30
CHAN="grid.update"

lat="$1"
lon="$2"
az="$3"

redis-cli set ${CHAN}:current \
          "{\"timestamp\": $(date +%s), \"lat\": $lat, \"lon\": $lon, \"azimuth\": $az}"

while true; do
    redis-cli publish "$CHAN" \
              "{\"timestamp\": $(date +%s), \"lat\": $lat, \"lon\": $lon, \"azimuth\": $az}"
    sleep $INTERVAL
done
