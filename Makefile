#
# Install GICS services
#
SHELL = /bin/bash
BINDIR = $(HOME)/bin
SVCDIR = $(HOME)/.config/systemd/user
CFGDIR = $(HOME)/.config/icex

PROGS := venv_helper.sh grid2serial.sh sattrack.py csv2lp.py \
         logrfgps.py
SVCS := archiver.service serialout.service rngfilt.service \
        gpsrdr.service metrdr.service weather-app.service \
        gridcheck.service gps-app.service survey-app.service \
	    sattrack.service logrfgps.service
CFGS := gps.yaml archivecfg.yaml met.yaml gridcheck.yaml ephem.yaml \
        gps-norf.yaml

.PHONY: install install-sv install-bin install-cfg

all: install

$(SVCS): %.service: %.service.in
	sed -e "s!@HOME@!$(HOME)!g" \
        -e "s!@BINDIR@!$(BINDIR)!g" \
        -e "s!@CFGDIR@!$(CFGDIR)!g" $< > $@

install: install-cfg install-sv install-bin

install-sv: $(SVCS) gics.target
	install -d $(SVCDIR)
	install -m 644 -t $(SVCDIR) $^
	systemctl --user enable $^

install-bin: $(PROGS)
	install -d $(BINDIR)
	install -m 755 -t $(BINDIR) $^

install-cfg: $(CFGS)
	install -d $(CFGDIR)
	install -m 644 -t $(CFGDIR) $^

clean:
	rm -f *.service
