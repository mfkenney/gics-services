#!/usr/bin/env python
"""
Log RF-GPS range and bearing errors to an InfluxDB database.
"""
import sys
import signal
import argparse
import redis
import logging
from urlparse import urlparse
import simplejson as json
from influxdb import InfluxDBClient


def signal_handler(signum, frame):
    sys.exit(0)


def process_data(pubsub, dbclient, measurement, logger):
    # Database record
    rec = {
        'measurement': measurement,
        'fields': {},
        'tags': {}
    }
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            logger.debug('> %r', msg['data'])
            try:
                d = json.loads(msg['data'])
            except Exception:
                logger.exception('Record decode error')
            else:
                rec['time'] = int(d['timestamp'] * 1e9)
                for ref in d['refs']:
                    r = dict(rec)   # clone the record
                    r['tags']['name'] = ref['name']
                    r['fields']['range'] = float(ref['range'])
                    r['fields']['bearing'] = float(ref['bearing'])
                    logger.debug('< %r', r)
                    dbclient.write_points([r])


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('chan',
                        help='Redis channel name for RF-GPS range data')
    parser.add_argument('dburl',
                        help='InfluxDB database URL')
    parser.add_argument('-m', '--measurement',
                        default='range_error',
                        help='InfluxDB measurement name (%(default)s)')
    parser.add_argument('--host',
                        default='localhost',
                        help='Redis host (%(default)s)')
    parser.add_argument('--port',
                        type=int,
                        default=6379,
                        help='Redis TCP port (%(default)d)')
    parser.add_argument('--db',
                        type=int,
                        default=0,
                        help='Redis DB number (%(default)d)')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logger = logging.getLogger('rfgps')
    logger.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(
        logging.Formatter('%(levelname)-8s %(message)s'))
    logger.addHandler(ch)

    u = urlparse(args.dburl)
    client = InfluxDBClient.from_DSN(u.geturl())
    client.create_database(u.path[1:], if_not_exists=True)
    logger.info('Connected to InfluxDB')

    rd = redis.StrictRedis(host=args.host,
                           port=args.port,
                           db=args.db)
    logger.info('Connected to Redis')

    signal.signal(signal.SIGTERM, signal_handler)
    pubsub = rd.pubsub()
    pubsub.subscribe(args.chan)

    try:
        process_data(pubsub, client, args.measurement, logger)
    except (KeyboardInterrupt, SystemExit):
        logger.info('Exiting ...')
    except Exception:
        logger.exception('Unhandled exception. Exiting')


if __name__ == '__main__':
    main()
