#!/usr/bin/env python
"""
Track the elevation a azimuth of one or more satellites relative to the
APLIS Camp location and publish the information on a Redis channel.
"""
import sys
import redis
import math
import ephem
import argparse
import logging
import Queue
import json
import signal
import yaml
from threading import Thread
from datetime import datetime
from decimal import Decimal


def signal_handler(signum, frame):
    sys.exit(0)


def read_gps(rd, channel, outq, interval=1, timevar='t'):
    """
    Read the Camp GPS position from a Redis channel and write
    it to a Queue.
    """
    pubsub = rd.pubsub()
    pubsub.subscribe(channel)
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            logging.debug('> %r', msg['data'])
            try:
                rec = json.loads(msg['data'], parse_float=Decimal)
            except Exception:
                logging.exception('Message decode error')
                continue
            if (rec[timevar] % interval) == 0:
                logging.debug('< %r', rec)
                outq.put((str(rec['lat']), str(rec['lon'])))


def main():
    """
    Track satellites relative to the current Grid origin.
    """
    parser = argparse.ArgumentParser(description=main.__doc__,
                                     epilog=__doc__)
    parser.add_argument('infile',
                        type=file,
                        help='YAML file with satellite TLE data')
    parser.add_argument('channel',
                        help='Redis channel name for output data')
    parser.add_argument('--host',
                        default='localhost',
                        help='Redis host (%(default)s)')
    parser.add_argument('--port',
                        type=int,
                        default=6379,
                        help='Redis TCP port (%(default)d)')
    parser.add_argument('--db',
                        type=int,
                        default=0,
                        help='Redis DB number (%(default)d)')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--gps-channel',
                        default='grid.update',
                        help='Redis channel for input GPS data')
    parser.add_argument('--interval', '-i',
                        type=int,
                        default=1,
                        metavar='SECS',
                        help='maximum output record interval')
    parser.add_argument('--time-var',
                        default='timestamp',
                        help='name of time variable in input message')

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    bodies = []
    for entry in yaml.load(args.infile):
        if isinstance(entry, dict):
            body = ephem.readtle(entry['name'],
                                 *(entry['tle'].split('\n')[0:2]))
            bodies.append(body)
        else:
            bodies.append(entry.__class__())

    base = ephem.Observer()
    base.elevation = 0

    q = Queue.Queue()
    task = Thread(target=read_gps,
                  args=(redis.StrictRedis(host=args.host,
                                          port=args.port,
                                          db=args.db),
                        args.gps_channel,
                        q),
                  kwargs={'interval': args.interval,
                          'timevar': args.time_var})
    task.daemon = True
    task.start()

    rd = redis.StrictRedis(host=args.host,
                           port=args.port,
                           db=args.db)
    logging.info('Connected to Redis')
    signal.signal(signal.SIGTERM, signal_handler)

    try:
        while True:
            base.lat, base.lon = q.get()
            base.date = datetime.utcnow()
            for body in bodies:
                body.compute(base)
                rec = dict(name=body.name,
                           azimuth=round(math.degrees(body.az), 1),
                           alt=round(math.degrees(body.alt), 1))
                logging.debug('< %r', rec)
                rd.publish(args.channel, json.dumps(rec))
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')


if __name__ == '__main__':
    main()
