#!/bin/bash
#
# Publish simulated ICEX data messages using an archive data file.
#

KEYS=()

send_message() {
    chan="$1"
    shift

    parts=()
    for k in "${KEYS[@]}";do
        if [[ "$1" = [a-zA-Z]* ]]; then
            parts+=("\"$k\": \"$1\"")
        else
            parts+=("\"$k\": $1")
        fi
        shift
    done

    msg=$(printf "%s," "${parts[@]}")
    redis-cli publish $chan "{${msg%,*}}"
    redis-cli set "${chan}:current" "{${msg%,*}}"
}

channel="$1"
[[ "$channel" ]] || {
    echo "Usage: $(basename $0) channel [T_scale]"
    exit 1
}
shift
scale="${1:-1}"

read line
IFS=, read -a KEYS < <(sed -e 's/time/timestamp/' <<< "$line")
ti=0
for k in "${KEYS[@]}"; do
    [[ "$k" == "timestamp" ]] && break
    ((ti++))
done

t0=
IFS=, read -a record
t0="${record[$ti]}"
send_message "$channel" "${record[@]}"

while IFS=, read -a record; do
    t="${record[$ti]}"
    dt=$((t - t0))
    ((dt > 0)) && {
        echo "Sleeping $((dt/scale)) seconds ..."
        sleep $((dt/scale))
    }
    t0=$t
    send_message "$channel" "${record[@]}"
done
