#!/bin/bash

. $HOME/.venvs/icex/bin/activate
export CFGDIR=$HOME/.config/icex

exec "$@"
